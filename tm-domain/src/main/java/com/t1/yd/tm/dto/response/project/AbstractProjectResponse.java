package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class AbstractProjectResponse extends AbstractResultResponse {

    @Nullable
    private ProjectDTO projectDTO;

    public AbstractProjectResponse(@Nullable final ProjectDTO projectDTO) {
        this.projectDTO = projectDTO;
    }

    public AbstractProjectResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
