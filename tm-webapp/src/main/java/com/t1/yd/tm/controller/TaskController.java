package com.t1.yd.tm.controller;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/task")
public class TaskController {

    private final TaskRepository taskRepository;

    private final ProjectRepository projectRepository;

    @Autowired
    public TaskController(@NotNull final TaskRepository taskRepository,
                          @NotNull final ProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @GetMapping("/create")
    public String create() {
        taskRepository.create();
        return "redirect:/tasks";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") final String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable("id") final String id) {
        final Task task = taskRepository.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-update");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectRepository.findAll());
        modelAndView.addObject("statusValues", Status.values());
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(@ModelAttribute("task") Task task) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

}
