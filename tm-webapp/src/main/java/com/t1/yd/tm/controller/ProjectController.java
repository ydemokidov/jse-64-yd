package com.t1.yd.tm.controller;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/project")
public class ProjectController {

    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectController(@NotNull final ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @GetMapping("/create")
    public String create() {
        projectRepository.create();
        return "redirect:/projects";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") final String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable("id") final String id) {
        final Project project = projectRepository.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-update");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statusValues", Status.values());
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(@ModelAttribute("project") Project project, BindingResult result) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

}
