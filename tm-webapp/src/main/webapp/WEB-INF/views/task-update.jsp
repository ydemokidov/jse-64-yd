<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/header.jsp"/>

<h3>TASK EDIT</h3>

<form:form action="/task/update/${task.id}" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <p>
    <div>Name:</div>
    <div><form:input type="text" path="name"/></div>
    </p>
    <p>
    <div>Description:</div>
    <div><form:input type="text" path="description"/></div>
    </p>
    <p>
    <div>Status:</div>
    <form:select path="status">
        <form:option value="${null}" label="-----"/>
        <form:options items="${statusValues}" itemLabel="displayName"/>
    </form:select>
    </p>
    <p>
    <div>Created:</div>
    <div><form:input type="date" path="created"/>
    </div>
    </p>
    <div>Project:</div>
    <form:select path="projectId">
        <form:option value="${null}" label="-----"/>
        <form:options items="${projects}" itemLabel="name" itemValue="id"/>
    </form:select>
    </p>
    <button type="submit" style="padding-top: 20px;">SAVE</button>
</form:form>

<jsp:include page="../include/footer.jsp"/>