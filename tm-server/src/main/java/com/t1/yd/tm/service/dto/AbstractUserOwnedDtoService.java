package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.IUserOwnedDtoService;
import com.t1.yd.tm.dto.model.AbstractUserOwnedEntityDTO;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.repository.dto.AbstractDtoUserOwnedJpaRepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import java.util.List;

public abstract class AbstractUserOwnedDtoService<E extends AbstractUserOwnedEntityDTO, R extends AbstractDtoUserOwnedJpaRepository<E>> extends AbstractDtoService<E, R> implements IUserOwnedDtoService<E> {

    @Autowired
    public AbstractUserOwnedDtoService(@NotNull final ILoggerService loggerService) {
        super(loggerService);
    }

    @Override
    @SneakyThrows
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        getRepository().save(entity);
        return entity;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        getRepository().deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        return getRepository().findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        return getRepository().findAllByUserId(userId, sort);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().findByUserIdAndId(userId, id).orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final E result = findOneById(userId, id);
        if (result == null) throw new EntityNotFoundException();
        getRepository().deleteByUserIdAndId(userId, id);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        return getRepository().count();
    }

}
