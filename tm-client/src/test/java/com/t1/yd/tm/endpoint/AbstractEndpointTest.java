package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.request.data.DataBackupLoadRequest;
import com.t1.yd.tm.dto.request.data.DataBackupSaveRequest;
import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IDataEndpoint dataEndpoint = IDataEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    public void loadBackup(final String token) {
        @NotNull DataBackupLoadRequest request = new DataBackupLoadRequest();
        request.setToken(token);
        dataEndpoint.backupLoad(request);
    }

    public void saveBackup(final String token) {
        @NotNull DataBackupSaveRequest request = new DataBackupSaveRequest();
        request.setToken(token);
        dataEndpoint.backupSave(request);
    }

    public String login(@NotNull final String login, @NotNull final String password) {
        UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return authEndpoint.login(request).getToken();
    }

    public void logout(final String token) {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(token);
        authEndpoint.logout(request);
    }

}
