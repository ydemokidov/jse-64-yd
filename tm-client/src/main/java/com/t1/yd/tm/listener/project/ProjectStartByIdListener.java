package com.t1.yd.tm.listener.project;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.project.ProjectStartByIdRequest;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProjectStartByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project_start_by_id";
    @NotNull
    public static final String DESCRIPTION = "Start project by Id";

    @Autowired
    public ProjectStartByIdListener(@NotNull final ITokenService tokenService,
                                    @NotNull final IProjectEndpoint projectEndpointClient) {
        super(tokenService, projectEndpointClient);
    }

    @Override
    @EventListener(condition = "@projectStartByIdListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest();
        request.setId(id);
        request.setToken(getToken());
        getProjectEndpointClient().startProjectById(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
