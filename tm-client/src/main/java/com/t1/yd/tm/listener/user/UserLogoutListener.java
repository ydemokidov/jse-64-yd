package com.t1.yd.tm.listener.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserLogoutListener extends AbstractUserListener {

    @NotNull
    private final String name = "user_logout";
    @NotNull
    private final String description = "User logout";

    @Autowired
    public UserLogoutListener(@NotNull final ITokenService tokenService,
                              @NotNull final IUserEndpoint userEndpointClient,
                              @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService, userEndpointClient, authEndpointClient);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userLogoutListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(getToken());
        getAuthEndpoint().logout(request);
        System.out.println("[USER LOGGED OUT]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
