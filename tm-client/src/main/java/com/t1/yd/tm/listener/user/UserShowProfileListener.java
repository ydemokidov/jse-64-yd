package com.t1.yd.tm.listener.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.dto.request.user.UserGetProfileRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserShowProfileListener extends AbstractUserListener {

    @NotNull
    private final String name = "user_show_profile";
    @NotNull
    private final String description = "Show user profile";

    @Autowired
    public UserShowProfileListener(@NotNull final ITokenService tokenService,
                                   @NotNull final IUserEndpoint userEndpointClient,
                                   @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService, userEndpointClient, authEndpointClient);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userShowProfileListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER PROFILE:]");

        @NotNull final UserGetProfileRequest request = new UserGetProfileRequest();
        request.setToken(getToken());

        @NotNull final UserDTO userDTO = getAuthEndpoint().getProfile(request).getUserDTO();

        showUser(userDTO);
        System.out.println("Last Name: " + userDTO.getLastName());
        System.out.println("First Name: " + userDTO.getFirstName());
        System.out.println("Middle Name: " + userDTO.getMiddleName());
        System.out.println("Role: " + userDTO.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
