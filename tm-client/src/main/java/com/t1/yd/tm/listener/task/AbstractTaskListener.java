package com.t1.yd.tm.listener.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    private final ITaskEndpoint taskEndpointClient;

    public AbstractTaskListener(@NotNull final ITokenService tokenService,
                                @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService);
        this.taskEndpointClient = taskEndpointClient;
    }

    protected ITaskEndpoint getTaskEndpointClient() {
        return taskEndpointClient;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(@NotNull final TaskDTO taskDTO) {
        System.out.println("ID: " + taskDTO.getId());
        System.out.println("NAME: " + taskDTO.getName());
        System.out.println("DESC: " + taskDTO.getDescription());
        System.out.println("STATUS: " + Status.toName(taskDTO.getStatus()));
    }

}