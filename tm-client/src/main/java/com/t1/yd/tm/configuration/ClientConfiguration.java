package com.t1.yd.tm.configuration;

import com.t1.yd.tm.api.endpoint.*;
import com.t1.yd.tm.api.service.IPropertyService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.t1.yd.tm")
public class ClientConfiguration {

    @NotNull
    private final IPropertyService propertyService;

    @Autowired
    public ClientConfiguration(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Bean
    public IUserEndpoint userEndpointClient() {
        return IUserEndpoint.newInstance(propertyService);
    }

    @Bean
    public IDataEndpoint dataEndpointClient() {
        return IDataEndpoint.newInstance(propertyService);
    }

    @Bean
    public ISystemEndpoint systemEndpointClient() {
        return ISystemEndpoint.newInstance(propertyService);
    }

    @Bean
    public ITaskEndpoint taskEndpointClient() {
        return ITaskEndpoint.newInstance(propertyService);
    }

    @Bean
    public IAuthEndpoint authEndpointClient() {
        return IAuthEndpoint.newInstance(propertyService);
    }

    @Bean
    public IProjectEndpoint projectEndpointClient() {
        return IProjectEndpoint.newInstance(propertyService);
    }

}
